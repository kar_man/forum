# Highlight Author

![License](https://img.shields.io/badge/license-0BSD-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/cavestudio/flarum-ext-hightlight-author.svg)](https://packagist.org/packages/cavestudio/flarum-ext-hightlight-author)

A [Flarum](http://flarum.org) extension. highlight post author

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require cavestudio/flarum-ext-hightlight-author
```

### Updating

```sh
composer update cavestudio/flarum-ext-hightlight-author
```

### Links

- [Packagist](https://packagist.org/packages/cavestudio/flarum-ext-hightlight-author)
