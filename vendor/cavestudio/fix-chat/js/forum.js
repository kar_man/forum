import app from 'flarum/app'
import { extend } from 'flarum/extend';
import DiscussionPage from 'flarum/components/DiscussionPage';
import Badge from 'flarum/components/Badge';
import User from 'flarum/models/User';

//add post creator badges
 app.initializers.add('cavestudio/fix-chat', function(){
    console.log('load');
    //id of this user
    let UserId = 0;
    //id of this discussion
    let DiscussId = 0;

    //get id of this discussion
    extend(DiscussionPage.prototype,'view',function(item){
        if(this.discussion){
            DiscussId = this.discussion.data.id;
        }
    });
    //add badges to author
    extend(User.prototype,'badges',function(badge){
        const User=this;
        if(UserId == 0){
            UserId = User.data.id;
        }

        //post author id
        let author = this.store.data.discussions[DiscussId].payload.included[0].relationships.user.data.id;
    
        //if this page is discussion
    if(app.current.bodyClass == 'App--discussion' && User.data.id == author){
        badge.add('post-creator', Badge.component({
            type: 'sticky',
            label: 'creator',
            icon: 'fas fa-pencil-alt'
          }));
    }
    });
})
