Hey <?php echo e($user->username); ?>,

<?php echo e($blueprint->actor->username); ?> added you to an existing private discussion, titled '<?php echo e($blueprint->discussion->title); ?>'.

View it here: <?php echo e(app()->url()); ?>/d/<?php echo e($blueprint->discussion->id); ?>-<?php echo e($blueprint->discussion->slug); ?> (You may need to login first)
